# Laravel Logger

Custom driver for laravel logging
### Requirement:
- Laravel 10.x
### Support:
- Discord
- Telegram (coming soon)

### How to use:
Config channel in `config/logging.php`

```
'discord' => [
    'driver' => 'custom',
    'via'    => \Thainph\LaravelLogger\Discord\DiscordLogger::class,
    'url' => env('LOG_DISCORD_WEBHOOK_URL'),
    'username' => env('APP_NAME'),
    'emoji' => ':boom:',
    'level' => env('LOG_LEVEL', 'critical'),
],
```