<?php
namespace Thainph\LaravelLogger\Enum;

enum Color: int
{
    case Debug = 1831177;

    /**
     * Interesting events
     *
     * Examples: User logs in, SQL logs.
     */
    case Info = 2431724;

    /**
     * Uncommon events
     */
    case Notice = 15579916;

    /**
     * Exceptional occurrences that are not errors
     *
     * Examples: Use of deprecated APIs, poor use of an API,
     * undesirable things that are not necessarily wrong.
     */
    case Warning = 13788426;

    /**
     * Runtime errors
     */
    case Error = 15026481;

    /**
     * Critical conditions
     *
     * Example: Application component unavailable, unexpected exception.
     */
    case Critical = 15927207;

    /**
     * Action must be taken immediately
     *
     * Example: Entire website down, database unavailable, etc.
     * This should trigger the SMS alerts and wake you up.
     */
    case Alert = 13443923;

    /**
     * Urgent alert.
     */
    case Emergency = 16319235;

}
