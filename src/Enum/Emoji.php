<?php
namespace Thainph\LaravelLogger\Enum;

enum Emoji: string
{
    case Debug = ':robot:';

    /**
     * Interesting events
     *
     * Examples: User logs in, SQL logs.
     */
    case Info = ':smiley:';

    /**
     * Uncommon events
     */
    case Notice = ':eyes:';

    /**
     * Exceptional occurrences that are not errors
     *
     * Examples: Use of deprecated APIs, poor use of an API,
     * undesirable things that are not necessarily wrong.
     */
    case Warning = ':cold_sweat:';

    /**
     * Runtime errors
     */
    case Error = ':hot_face:';

    /**
     * Critical conditions
     *
     * Example: Application component unavailable, unexpected exception.
     */
    case Critical = ':fire:';

    /**
     * Action must be taken immediately
     *
     * Example: Entire website down, database unavailable, etc.
     * This should trigger the SMS alerts and wake you up.
     */
    case Alert = ':rotating_light:';

    /**
     * Urgent alert.
     */
    case Emergency = ':sos:';

}
