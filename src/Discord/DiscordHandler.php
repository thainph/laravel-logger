<?php
namespace Thainph\LaravelLogger\Discord;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\LogRecord;
use Monolog\Level;
use Thainph\LaravelLogger\Enum\Color;
use Thainph\LaravelLogger\Enum\Emoji;

class DiscordHandler extends AbstractProcessingHandler
{
    private string $webhookUrl;

    public function __construct(string $webhookUrl, $level = Level::Critical, bool $bubble = true)
    {
        parent::__construct($level, $bubble);
        $this->webhookUrl = $webhookUrl;
    }

    /**
     * {@inheritDoc}
     */
    protected function write(LogRecord $record): void
    {
        $title = $record->level->getName() . ' - ' . config('app.env');
        $context = $this->getContext($record);

        $this->sendLogAsMessage(
            $this->getEmoji($record->level),
            $record->message,
            $title,
            $this->getBlockColor($record->level),
            $record->datetime
        );

        if ($context) {
            $this->sendLogAsFile($context);
        }
    }

    protected function sendLogAsFile($description): void
    {
        $tmpLogFile = $this->createTmpLogFile($description);
        $payload = [
            'name'     => 'file',
            'contents' => fopen($tmpLogFile, 'r'),
            'filename' => basename($tmpLogFile),
        ];
        Http::asMultipart()->post($this->webhookUrl, $payload);
        unlink($tmpLogFile);
    }

    protected function sendLogAsMessage($emoji, $message, $title, $color, $timestamp): void
    {
        $payload = [
            'content' => $emoji,
            'embeds' => [
                [
                    'title' => $title,
                    'description' => '```'. $message . '```',
                    'color' => $color,
                    'timestamp' => $timestamp,
                    'fields' => [
                        [
                            'name' => 'Project',
                            'value' => config('app.name'),
                            'inline' => true
                        ],
                        [
                            'name' => 'Domain',
                            'value' => config('app.url'),
                            'inline' => true
                        ]
                    ]
                ]
            ],
        ];

        Http::asJson()->post($this->webhookUrl, $payload);
    }

    protected function getContext(LogRecord $record): ?string
    {
        $context = $record->context;

        if (!empty($context)) {
            return !empty($context['exception'])
                ? json_encode($context['exception']->getTrace())
                : json_encode($record['context']);
        }

        return null;
    }
    protected function getBlockColor(Level $level): Color
    {
        return match ($level) {
            Level::Emergency => Color::Emergency,
            Level::Critical => Color::Critical,
            Level::Error => Color::Error,
            Level::Alert => Color::Alert,
            Level::Warning => Color::Warning,
            Level::Notice => Color::Notice,
            Level::Info => Color::Info,
            default => Color::Debug,
        };
    }

    protected function getEmoji(Level $level): Emoji
    {
        return match ($level) {
            Level::Emergency => Emoji::Emergency,
            Level::Critical => Emoji::Critical,
            Level::Error => Emoji::Error,
            Level::Alert => Emoji::Alert,
            Level::Warning => Emoji::Warning,
            Level::Notice => Emoji::Notice,
            Level::Info => Emoji::Info,
            default => Emoji::Debug,
        };
    }
    protected function createTmpLogFile($content): string
    {
        $logFile = storage_path('logs/'. Str::random() . '.json');
        file_put_contents($logFile, $content);

        return $logFile;
    }
}
