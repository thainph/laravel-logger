<?php

namespace Thainph\LaravelLogger\Discord;

use Monolog\Logger;

class DiscordLogger
{
    public function __invoke(array $config): Logger
    {
        return new Logger('discord', [
            new DiscordHandler($config['url'], $config['level'] ?? null
        )]);
    }
}
